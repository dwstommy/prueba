package ejemplo.hola;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan
public class HolaApplicationMain {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(HolaApplicationMain.class, args);
    }
}