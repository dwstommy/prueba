package ejemplo.hola.config;

import ejemplo.hola.endpoints.HolaResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig  extends ResourceConfig {

    public JerseyConfig() {
        register(HolaResource.class);
    }
}