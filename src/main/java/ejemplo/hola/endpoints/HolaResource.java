package ejemplo.hola.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/saludo")
public class HolaResource {

    @GET
    public String getSaludo() {
        return "Hola, Edwin!";
    }

}
